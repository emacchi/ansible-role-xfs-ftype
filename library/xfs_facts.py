#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Ansible Project
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type


ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: xfs_facts
author:
  - Sam Doran (@samdoran)
version_added: '2.6'
short_description: Gather ouput of C(xfs_info) on all XFS mounts
description:
  - Gather information on all XFS mounts on a system
'''

EXAMPLES = '''
- name: Gather XFS facts
  xfs_facts:

# Example output
# "xfs_facts": [
#         {
#             "agcount": "4",
#             "agsize": "3276800",
#             "ascii-ci": "0",
#             "attr": "2",
#             "blocks": "0",
#             "bsize": "4096",
#             "crc": "1",
#             "data": "",
#             "extsz": "4096",
#             "finobt": "0",
#             "ftype": "1",
#             "imaxpct": "25",
#             "isize": "512",
#             "lazy-count": "1",
#             "log": "internal",
#             "meta-data": "/dev/mapper/centos_centos7-root",
#             "mount": "/",
#             "naming": "version2",
#             "projid32bit": "1",
#             "realtime": "none",
#             "rtextents": "0",
#             "sectsz": "512",
#             "spinodes": "0",
#             "sunit": "0",
#             "swidth": "0",
#             "version": "2"
#         },
#         {
#             "agcount": "4",
#             "agsize": "65536",
#             "ascii-ci": "0",
#             "attr": "2",
#             "blocks": "0",
#             "bsize": "4096",
#             "crc": "1",
#             "data": "",
#             "extsz": "4096",
#             "finobt": "0",
#             "ftype": "1",
#             "imaxpct": "25",
#             "isize": "512",
#             "lazy-count": "1",
#             "log": "internal",
#             "meta-data": "/dev/sda1",
#             "mount": "/boot",
#             "naming": "version2",
#             "projid32bit": "1",
#             "realtime": "none",
#             "rtextents": "0",
#             "sectsz": "512",
#             "spinodes": "0",
#             "sunit": "0",
#             "swidth": "0",
#             "version": "2"
#         },
#         {
#             "agcount": "4",
#             "agsize": "1834496",
#             "ascii-ci": "0",
#             "attr": "2",
#             "blocks": "0",
#             "bsize": "4096",
#             "crc": "1",
#             "data": "",
#             "extsz": "4096",
#             "finobt": "0",
#             "ftype": "1",
#             "imaxpct": "25",
#             "isize": "512",
#             "lazy-count": "1",
#             "log": "internal",
#             "meta-data": "/dev/mapper/centos_centos7-home",
#             "mount": "/home",
#             "naming": "version2",
#             "projid32bit": "1",
#             "realtime": "none",
#             "rtextents": "0",
#             "sectsz": "512",
#             "spinodes": "0",
#             "sunit": "0",
#             "swidth": "0",
#             "version": "2"
#         }
#     ]
# }
'''

RETURN = '''
xfs_facts:
  description: >
    List of dictionaries containing the output of C(xfs_info) for each mount
    found in C(/proc/mounts)
  returned: always
  type: complex
'''

import re

from ansible.module_utils.basic import AnsibleModule


def get_mounts():
    with open('/proc/mounts', 'r') as f:
        lines = f.readlines()

        xfs_mounts = []
        xfs_pattern = re.compile(r'\bxfs\b')

        for line in lines:
            if xfs_pattern.search(line):
                mount = line.split()[1]
                xfs_mounts.append(mount)
        return xfs_mounts


def get_mount_facts(module, xfs_bin, mount):
    xfs_command = [xfs_bin, mount]
    rc, out, err = module.run_command(xfs_command)

    if rc != 0:
        module.fail_json(msg=err)

    mount_facts = []

    for line in out.splitlines():

        # Make sure everything is k=v
        line = re.sub(r'(\w+)\s+\=', r'\1=', line)

        # Remove spaces in values, such as 'name=version 2'
        line = re.sub(r'=(\w+)\s+(\d+)', r'=\1\2', line)

        # Split k=v groups on comma and spaces
        fields = re.split(r',?\s+', line)
        fields = filter(None, fields)

        # Clean out '=' values
        fields = [i for i in fields if i != '=']

        mount_facts.append(fields)

    # Flatten into one list
    flat_list = []
    for sublist in mount_facts:
        for item in sublist:
            flat_list.append(item)

    # Clean out 'blks'
    flat_list = [i for i in flat_list if i != 'blks']

    # Convert k=v strings into dictionary
    mount_facts_dict = {'mount': mount}
    for i in flat_list:
        x = i.split('=')
        mount_facts_dict[x[0]] = x[1]

    return mount_facts_dict


def main():
    module = AnsibleModule(
        argument_spec=dict(
            mount=dict(type='str', default='/')
        ),

        supports_check_mode=True
    )

    xfs_bin = module.get_bin_path('xfs_info', required=True)
    mounts = get_mounts()
    xfs_facts = []

    for mount in mounts:
        xfs_facts.append(get_mount_facts(module, xfs_bin, mount))

    results = dict(
        ansible_facts=dict(
            xfs_facts=list(xfs_facts)
        )
    )

    module.exit_json(**results)


if __name__ == '__main__':
    main()
