xfs-ftype
======================

Group hosts by XFS ftype based on gathered XFS facts. Hosts will be place into groups named `xfs-ftype[n]` if any xfs mounts are found on the system. If no XFS mounts are found on the system, hosts will be placed in the `not-xfs` group.

Optionally generate a static inventory file containing the XFS host groups.

Requirements
------------

None.

Role Variables
--------------

| Name                            | Default Value       | Description                                  |
|---------------------------------|---------------------|----------------------------------------------|
| `generate_xfs_static_inventory` | `no`                | Whether to generate a static inventory file. |

Dependencies
------------

None.

Exaple Playbooks
----------------

```yaml
- name: Group hosts based on XFS facts
  hosts: all

  roles:
    - xfs-ftype

- name: Target hosts with XFS ftype 1
  hosts: all:&xfs-ftype1
  become: yes

  tasks:
    - ping:

- name: Target hosts with XFS ftype 0
  hosts: all:*xfs-ftype0

  tasks:
    - ping:

- name: Target hosts with no XFS mounts
  hosts: all:&not-xfs

  tasks:
    - ping:

```

Generate a static inventory (Not recommended. Generating static inventory files from dynamic sources is considered an anti-pattern that should be avoided.)
```yaml
- name: Group hosts based on XFS facts and generate static inventory file
  hosts: all

  roles:
    - name: xfs-ftype
      vars:
        generate_xfs_static_inventory: yes
```

License
-------

Apache 2.0

TODO
----

* Create tasks to format disks with the right XFS configuration

Thanks
------

I want to thank @dmsimard for his role which inspired me:
https://github.com/dmsimard/ansible-meltdown-spectre-inventory/
